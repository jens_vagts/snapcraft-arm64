# Snapcraft Arm64

Snapcraft docker image for arm64 CPU architectures for building arm64 snaps with docker.

Sources used in this project:
- Using buildx in gitlab ci: [Cross build your docker images](https://aapjeisbaas.nl/post/cross-build-your-docker-images/)
- Official buildx documentation: [Docker Buildx](https://docs.docker.com/buildx/working-with-buildx/)
- Offical snapcraft docker image sources: [snapcore/snapcraft](https://github.com/snapcore/snapcraft)
